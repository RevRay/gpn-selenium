package ru.gpn.selenium.listeners;

import org.testng.ITestListener;
import org.testng.ITestResult;
import ru.gpn.selenium.core.WDWrapper;

public class FailScreenshotListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("### Started test execution");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        WDWrapper.getInstance("chrome").takeScreenshot();
    }
}
