package ru.gpn.selenium;

import com.fasterxml.jackson.databind.ser.Serializers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;
import ru.gpn.selenium.pages.MainPage;
import ru.gpn.selenium.pages.SearchResultsPage;
import ru.gpn.selenium.pages.TargetPage;

/**
 * Тестовый сценарий для поисковика Яндекс.
 * 1. Открыть страницу yandex.ru
 * 2. Проверить, отображаются ли над поисковой строкой кнопки "Маркет", "Видео", "Картинки", "Карты" (проверяется наличие элементов логотип + текст).
 * 3. Ввести в поле поиска запрос "porsche 356B 1:18 model"
 * 4. Нажать кнопку найти
 * 5. Проверить, что по данному поисковому запросу получено как минимум два результата
 * 6. Проверить, что по данному поисковому запросу есть как минимум 3 страницы результатов
 * 7. Перейти на 3-ю страницу результатов
 * 8. Проверить, что мы все еще находимся в поисковике Яндекс (URL)
 * 9. Открыть 2-й поисковый результат в выдаче на данной странице
 * 10. Убедиться что произошел переход со страницы поисковика на целевую страницу
 */
public class YandexPageObjectStyleTest2 extends BaseTest {

    @Test
    public void testSearch() {
        //Fluent
        MainPage mainPage = new MainPage(browserType);
        mainPage.open();
        mainPage.checkAllNavigationMenuSectionsPresent();
//                .enterSearchQuery("")
        mainPage.enterSearchQuery("Коврижки со скидкой");
        mainPage.clickSearchButton();

        SearchResultsPage searchResultsPage = new SearchResultsPage(browserType);

        int resultsCount = searchResultsPage.getSearchResultsCount();
        int pagesCount = searchResultsPage.getSearchPageCount();
        Assert.assertTrue(resultsCount >= 2);
        Assert.assertTrue(pagesCount > 2);


        searchResultsPage.goToResultsPage(3);
        Assert.assertTrue(searchResultsPage.getCurrentUrl().contains("yandex.ru/search"));

        searchResultsPage.clickSearchResultByIndex(2);

        TargetPage targetPage = new TargetPage(browserType);
        String currentUrl = targetPage.getCurrentUrl();
        Assert.assertFalse(currentUrl.startsWith("https://yandex.ru"));

    }

    //SOLID S - single responsibility principle   Gods Object

    //PO - model for our app
}
