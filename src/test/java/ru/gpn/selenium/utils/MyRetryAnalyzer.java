package ru.gpn.selenium.utils;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class MyRetryAnalyzer implements IRetryAnalyzer {

    int count = 0;
    final int maxAttempts = 3;

    @Override
    public boolean retry(ITestResult result) {
        if (count < maxAttempts) {
            count++;
            return true;
        }
        return false;
    }
}
