package ru.gpn.selenium;

import org.testng.annotations.Test;
import ru.gpn.selenium.utils.MyRetryAnalyzer;

public class FlakyTest {

    @Test(description = "my flaky test",
    retryAnalyzer = MyRetryAnalyzer.class)
    public void flakyTest() {
        double random = Math.random(); //0 , 1

        if (random > 0.1) {
            throw new AssertionError();
        }
    }

}
