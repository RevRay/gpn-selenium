package ru.gpn.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDemo {
    public static void main(String[] args) {
        String URL = "https://ya.ru";

        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        ChromeDriver driver = new ChromeDriver();
        driver.get(URL);

        driver.manage().window().maximize();

        WebElement searchField = driver.findElement(By.id("text"));
        searchField.sendKeys("Коврижки со скидкой");

//        driver.findElement(By.xpath("/html/body/main/div[2]/form/div[2]/button"));
        WebElement searchButton = driver.findElement(By.xpath("//button[@aria-label=\"Найти\"]"));//XPath - зык запросов для XMl структур
        searchButton.click();

        driver.quit(); //закроет все окна хрома, которые были открыты с помощью драйвера
//        driver.close(); //закрыть текущее вкладку
    }
}
