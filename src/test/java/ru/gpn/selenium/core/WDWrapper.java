package ru.gpn.selenium.core;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.gpn.selenium.YandexTest;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Set;

//Singleton
//1. private field with single object instance
//2. private constructor
//3. method to access single instance
public class WDWrapper implements WebDriver {

    //singleton internals
    private static ThreadLocal<WDWrapper> wd = new ThreadLocal<>();

    private WDWrapper(String browserType) {
        switch (browserType) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("excludeSwitches",
                        Collections.singletonList("enable-automation"));


                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(options);

//                DesiredCapabilities capabilities = new DesiredCapabilities();
//                capabilities.setBrowserName("chrome");
//                capabilities.setCapability("enableVNC",true); //enables viewing tests in Selenoid UI
//                capabilities.setCapability("enableVideo", true);

//                capabilities.setVersion("103.0");

//                try {
//                    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
                break;

            case "firefox":
                driver = WebDriverManager.firefoxdriver().create();
                break;
            case "safari":
                break;
        }

        driver.manage().window().maximize();

        wait = new WebDriverWait(driver, Duration.ofMillis(10000), Duration.ofMillis(250));
    }

    public static WDWrapper getInstance(String browserType) {
        //List list;
        //list.add
        if (wd.get() == null) {
            wd.set(new WDWrapper(browserType));
        }
        return wd.get();
    }
    //----------

    final Logger LOG = LogManager.getLogger(WDWrapper.class);

    public WebDriver driver; //heart
    public WebDriverWait wait;

    @Override
    public void get(String url) {
        LOG.debug("Opening URL: '{}'", url);
        driver.get(url);
    }

    @Override
    public String getCurrentUrl() {
        LOG.debug("Getting current url...");
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        String title = driver.getTitle();
        LOG.debug("Current title is '{}'", title);
        return title;
    }

//    @Attachment(value = "fail-screenshot", type = "image/png")
    public byte[] takeScreenshot() {
        //WebDriver
        //RemoteWebDriver
        //Chrome FF Saf
        byte[] imageBytes = ((RemoteWebDriver) driver).getScreenshotAs(OutputType.BYTES);
        Allure.addAttachment("fail-screenshot", String.valueOf(imageBytes), "image/png");
        File file = ((RemoteWebDriver) driver).getScreenshotAs(OutputType.FILE);
        try {
            File screenshotFolder = new File("screenshots");

            if (!screenshotFolder.exists()) {
                screenshotFolder.mkdirs(); //создание директории
            }

            FileHandler.copy(file, new File("screenshots/1.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imageBytes;
    }

    @Override
    public List<WebElement> findElements(By by) {
//        try {
            LOG.debug("Finding elements by xpath: " + by);
            List<WebElement> searchedElements = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
            LOG.trace("Found {} elements.", searchedElements.size());
            highlightElement(searchedElements.toArray(WebElement[]::new));
            return searchedElements;
//        } catch (NoSuchElementException | TimeoutException e) {
//            takeScreenshot();
//            throw e;
//        }
    }

    @Override
    public WebElement findElement(By by) {
//        try {
            LOG.debug("Finding element by xpath: " + by);
            WebElement searchedElement = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
            highlightElement(searchedElement);
            return searchedElement;
//        } catch (NoSuchElementException | TimeoutException e) {
//            takeScreenshot();
//            throw e;
//        }
    }

    @Override
    public String getPageSource() {
        return null;
    }

    @Override
    public void close() {

    }

    @Override
    public void quit() {

    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return null;
    }

    @Override
    public Options manage() {
        return null;
    }

    //highlight element
    public void highlightElement(WebElement... e) {
        ((JavascriptExecutor) driver).executeScript(
                String.format("arguments[0].style.backgroundColor='%s'",
                        "rgb(200,200,255)"),
                e
        );
    }

    //e2e
    public void clickElementByJS(WebElement element) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].click()", element);
    }

}
