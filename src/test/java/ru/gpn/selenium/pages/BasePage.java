package ru.gpn.selenium.pages;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import ru.gpn.selenium.YandexTest;
import ru.gpn.selenium.core.WDWrapper;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class BasePage {
    WebDriver driver;
    final Logger LOG = LogManager.getLogger(BasePage.class);

    public String URL;

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public BasePage(String browserType) {
        driver = WDWrapper.getInstance(browserType);
    }

    @Step("Открываю страницу...")
    public BasePage open() {
        LOG.info("Opening page by URL: " + URL);
        driver.get(URL);
        return this;
    }

    @Step("Переключаюсь на соседнюю вкладку...")
    public BasePage switchToAnotherTab() {
        Set<String> windowHandles = driver.getWindowHandles();
        String currentWindowHandle = driver.getWindowHandle();
        windowHandles.remove(currentWindowHandle);
        ArrayList<String> arrayList = new ArrayList(windowHandles);

        driver.switchTo().window(arrayList.get(0));
        return this;
    }

    @Step("Переключаюсь на вкладку с названием '{0}'")
    public BasePage switchToTabByTitleName(String searchedTitleName) {
        Set<String> windowHandles = driver.getWindowHandles();

        for (String windowHandle : windowHandles) {
            driver.switchTo().window(windowHandle);
            if (driver.getTitle().equals(searchedTitleName)) {
                return this;
            }
        }

        throw new RuntimeException(); //NoSuchTabException
    }

}
