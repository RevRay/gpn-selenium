package ru.gpn.selenium.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class SearchResultsPage extends BasePage{

    String pagesXpath = "//div[@class='pager__items']/*";
    String searchResultsXpath = "//ul[@id='search-result']/li/div";
    String searchResultsXpathTemplate = "//ul[@id='search-result']/li[%d]/div/div/a";
    String pageXpathTemplate = "//a[@aria-label='Страница %d']";

    public SearchResultsPage(String browser) {
        super(browser);
        this.URL = "https://yandex.ru/search";
    }

    @Step("Получаю количество поисковых результатов")
    public int getSearchResultsCount() {
        List<WebElement> searchResults = driver.findElements(By.xpath(searchResultsXpath));
        LOG.info("Search results pages count: " + searchResults.size());

        return searchResults.size();
    }

    @Step("Получаю количество страниц с поисковыми результатами")
    public int getSearchPageCount() {
        List<WebElement> searchPages = driver.findElements(By.xpath(pagesXpath));
        LOG.info("Search results pages count: " + searchPages.size());
        return searchPages.size();
    }

    @Step("Переходим на страницу с результатами номер '{pageNumber}'")
    public void goToResultsPage(int pageNumber) {
        LOG.info("Clicking search results page number #" + pageNumber);
        WebElement searchPageButton = driver.findElement(By.xpath(String.format(pageXpathTemplate, pageNumber)));
        searchPageButton.click();
    }

    @Step("Переходим на поисковый результат номер '{pageNumber}'")
    public void clickSearchResultByIndex(int index) {
        LOG.info("Clicking search result number #" + index);
        WebElement searchPageButton = driver.findElement(By.xpath(String.format(searchResultsXpathTemplate, index)));
        searchPageButton.click();

        this.switchToAnotherTab();
    }
}
