package ru.gpn.selenium.pages;

import io.qameta.allure.Step;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import ru.gpn.selenium.YandexTest;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainPage extends BasePage {

    String searchButtonXpath = "//div[@class='search2__button']/button";
    String searchFieldXpath = "//input[@id='text']";

    public enum Menu {
        MARKET("market"),
        VIDEO("video"),
        IMAGES("images"),
        MAPS("maps");

        String name;

        Menu(String name) {
            this.name = name;
        }
    }

    public MainPage(String browser) {
        super(browser);
        this.URL = "https://yandex.ru";
    }

    @Step("Кликнул на кнопку поиска")
    public MainPage clickSearchButton() {
        LOG.info("Clicking search button.");
        WebElement searchButton = driver.findElement(By.xpath(searchButtonXpath));//XPath - язык запросов для XMl структур
        searchButton.click();
        return this;
    }

    @Step("Ввел поисковый запрос")
    public MainPage enterSearchQuery(String searchQuery) {
        LOG.info("Entering search query: " + searchQuery);
        WebElement searchField = driver.findElement(By.xpath(searchFieldXpath));
        searchField.sendKeys(searchQuery);
        return this;
    }

    @Step("Проверили наличие критических секций меню")
    public MainPage checkAllNavigationMenuSectionsPresent() {
        LOG.info("Checking critical menu sections present.");

        String marketMenuItemXpathTemplate = "//a[@data-id='%s']/div[contains(@class, 'services-new__item-title')]";
        String marketMenuLogoXpathTemplate = "//a[@data-id='%s']/div[contains(@class, 'services-new__icon')]";


        for (Menu menuItemName : Menu.values()) {
            String marketMenuItemLocator = String.format(marketMenuItemXpathTemplate, menuItemName.name);
            String marketMenuLogoLocator = String.format(marketMenuLogoXpathTemplate, menuItemName.name);

            WebElement marketTitle = driver.findElement(By.xpath(marketMenuItemLocator));

            WebElement marketLogo = driver.findElement(By.xpath(marketMenuLogoLocator));

            Assert.assertTrue(marketLogo.isDisplayed());
            Assert.assertTrue(marketTitle.isDisplayed());
        }

        return this;
    }




}
