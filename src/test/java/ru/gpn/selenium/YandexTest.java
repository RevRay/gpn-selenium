package ru.gpn.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

/**
 * Тестовый сценарий для поисковика Яндекс.
 * 1. Открыть страницу yandex.ru
 * 2. Проверить, отображаются ли над поисковой строкой кнопки "Маркет", "Видео", "Картинки", "Карты" (проверяется наличие элементов логотип + текст).
 * 3. Ввести в поле поиска запрос "porsche 356B 1:18 model"
 * 4. Нажать кнопку найти
 * 5. Проверить, что по данному поисковому запросу получено как минимум два результата
 * 6. Проверить, что по данному поисковому запросу есть как минимум 3 страницы результатов
 * 7. Перейти на 3-ю страницу результатов
 * 8. Проверить, что мы все еще находимся в поисковике Яндекс (URL)
 * 9. Открыть 2-й поисковый результат в выдаче на данной странице
 * 10. Убедиться что произошел переход со страницы поисковика на целевую страницу
 */
public class YandexTest {

    @Test
    public void testSearch() {
        Logger LOG = LogManager.getLogger(YandexTest.class);

        String URL = "https://yandex.ru";

        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("excludeSwitches",
                Collections.singletonList("enable-automation"));

        ChromeDriver driver = new ChromeDriver(options);

        //1. Открыть страницу yandex.ru
        driver.manage().window().maximize();
        driver.get(URL);


        //2. Проверить, отображаются ли над поисковой строкой кнопки
        // "Маркет", "Видео", "Картинки", "Карты" (проверяется наличие элементов логотип + текст).

        List<String> menuItemNames = List.of("market", "images", "video", "maps");

        String marketMenuItemXpathTemplate = "//a[@data-id='%s']/div[contains(@class, 'services-new__item-title')]";
        String marketMenuLogoXpathTemplate = "//a[@data-id='%s']/div[contains(@class, 'services-new__icon')]";


        for (String menuItemName : menuItemNames) {
            LOG.debug("Finding element by xpath: " + String.format(marketMenuItemXpathTemplate, menuItemName));
            LOG.debug("Finding element by xpath: " + String.format(marketMenuLogoXpathTemplate, menuItemName));

            WebElement marketTitle = driver.findElement(By.xpath(String.format(marketMenuItemXpathTemplate, menuItemName)));
            WebElement marketLogo = driver.findElement(By.xpath(String.format(marketMenuLogoXpathTemplate, menuItemName)));

            Assert.assertTrue(marketLogo.isDisplayed());
            Assert.assertTrue(marketTitle.isDisplayed());
        }

        // 3. Ввести в поле поиска запрос "Коврижки со скидкой"
        WebElement searchField = driver.findElement(By.xpath("//input[@id='text']"));
        searchField.sendKeys("Коврижки со скидкой");

        //4. Нажать кнопку найти
//        WebElement searchButton = driver.findElement(By.xpath("//span[text()='Найти']"));//XPath - зык запросов для XMl структур
        WebElement searchButton = driver.findElement(By.xpath("//div[@class='search2__button']/button"));//XPath - зык запросов для XMl структур
        searchButton.click();

        // * 5. Проверить, что по данному поисковому запросу получено как минимум два результата

        List<WebElement> searchResults = driver.findElements(By.xpath("//ul[@id='search-result']/li"));
        Assert.assertTrue(searchResults.size() >= 2);


        // * 6. Проверить, что по данному поисковому запросу есть как минимум 3 страницы результатов
        List<WebElement> searchPages = driver.findElements(By.xpath("//div[@class='pager__items']/*"));
        Assert.assertTrue(searchPages.size() > 2);

        driver.quit();
    }
}
