package ru.gpn.selenium;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class BaseTest {

    String browserType;

    @BeforeTest
    @Parameters({"browser"})
    public void setUpTest(@Optional("chrome") String browserType) {
        this.browserType = browserType;
    }

}
