package ru.gpn.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class WebComponentTest {

    WebDriver driver;

    @BeforeClass
    public void setUp() { //setUp tearDown
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

    }

    @Test
    public void testAlerts() {
        String URL = "https://the-internet.herokuapp.com/";
        driver.get(URL);
        driver.findElement(By.xpath("//a[text()='JavaScript Alerts']")).click();

        String buttonAlertXpath = "//button[text()='Click for JS Alert']";
        String buttonConfirmXpath = "//button[text()='Click for JS Confirm']";
        String buttonPromptXpath = "//button[text()='Click for JS Prompt']";


        WebElement element = driver.findElement(By.xpath(buttonPromptXpath));
        element.click();

//        driver.switchTo().alert().accept();
//        driver.switchTo().alert().dismiss();
        driver.switchTo().alert().sendKeys("SDFSDFSDF");

        driver.switchTo().alert().accept();
    }

    @Test
    public void testIframe() {
        String URL = "https://the-internet.herokuapp.com/iframe";
        driver.get(URL);

        driver.switchTo().frame("mce_0_ifr");

        WebElement field = driver.findElement(By.xpath("//body[@id='tinymce']"));
        field.clear();
        field.sendKeys("43434343434");

        driver.switchTo().parentFrame();

        driver.findElement(By.xpath("//button[@aria-label='Align center']")).click();
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
